// Ticket Verification - DEBUG ==========================
'use strict'
var crypto = require('crypto');
var fs = require('fs');

var privatePem = fs.readFileSync(process.env.PWD + '/includes/certs/private.pem');
var key = privatePem.toString();
var sign = crypto.createSign('RSA-SHA1');
var data = "8C1C2";
sign.update(data);

var signMsg = sign.sign(key,'hex'); 
console.log(signMsg);

var publicPem = fs.readFileSync(process.env.PWD + '/includes/certs/public.pub');
var pubKey = publicPem.toString();

var verify = crypto.createVerify('RSA-SHA1');
verify.update(data);
var bool = verify.verify(pubKey, signMsg, 'hex');
console.log(bool);
