'use strict';
var models = require("./models");
var Promise = require('bluebird');
var auth = require("basic-auth");
var modelExtensions = require("./includes/modelExtensions.js");
var lines = modelExtensions.lines;
var stations = modelExtensions.stations;

/*
var Sequelize = require('sequelize');
var sequelize = new Sequelize('postgres://postgres:300309@localhost:5432/cmovDB');
sequelize.sync(true);
*/
var app = require('connect')();
var auth = require('basic-auth');
var http = require('http');
var swaggerTools = require('swagger-tools');

var serverPort = 8080;




// swaggerRouter configuration
var options = {
  swaggerUi:'/swagger.json',
  controllers: './controllers',
  useStubs: process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
};




// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var swaggerDoc = require('./api/swagger.json');

if(process.env.NODE_ENV == 'development'){
  swaggerDoc.host = "localhost:5000";
}

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  // Start the server
  
  models.sequelize.sync({force:true}).then(function () {
    var port = process.env.PORT || 5000;
    http.createServer(app).listen(port, function () {
      console.log('Your server is listening on port %d (http://localhost:%d)', port, port);
      console.log('Swagger-ui is available on http://localhost:%d/docs', port);


      // USERNAMES
      var usernames_passwords = [
        ["vmineiro", 1234,false],
        ["rodolfoaar", 1234,false],
        ["jpmoreira", 1234,false],
        ["danielasmendonca", 1234,true]];

      Promise.map(usernames_passwords,function(user) {
        models.User.create({"userName":user[0], "password":user[1],"conductor":user[2]});
      });

      // CARDS
      var cards = [
        ["card_1", "masterCard","12341233123412334", "2015","12","vmineiro"],
        ["card_2", "masterCard","90876543211234344", "2015","12","vmineiro"],
        ["card_3", "masterCard","75487292764819262", "2015","12","jpmoreira"]
      ];

      Promise.map(cards, function(card) {
        models.Card.create({"code":card[0],"type":card[1],"number":card[2],"expirationYear":card[3],"expirationMonth":card[4]})
            .then(function(cardObject) {

              models.User.findOne({"where":{"userName":card[5]}}).then(function(user){

                user.addCard(cardObject);

              });

            });
      });

      

      
      var trains = ["8:00","10:30","17:30","20:00"];

      Promise.map(lines,function(line){
        Promise.map(trains,function(trainTime){
            return Promise.all([
              models.Train.create(
                {"startTime":trainTime,"line":line.name,"direction":true}),
              models.Train.create(
                {"startTime":trainTime,"line":line.name,"direction":false})
              ]);
        });
      });
      
      // TICKETS
      var tickets = [
        ["2164363", 170,"2015-11-8","vmineiro","10:30","A1","A2"],
        ["1325636", 240,"2015-11-8","jpmoreira","10:30","C2","Central Station"],
        ["7695696", 320,"2015-11-8","rodolfoaar","8:00","B1","B2"],
        ["2351626", 400,"2015-11-8","jpmoreira","20:00","Central Station","C1"]];

      Promise.map(tickets, function(ticket) {
        models.Ticket.create({"code":ticket[0],"price":ticket[1],"date":ticket[2],"startStation":ticket[5],"endStation":ticket[6]})
            .then(function(ticketObject) {

              models.User.findOne({"where":{"userName":ticket[3]}}).then(function(user){

                user.addTicket(ticketObject);

              });

              // ADICIONAR COMBOIO AO BILHETE
              models.Train.findOne({"where":{"time":ticket[4]}}).then(function(train){

                train.addTicket(ticketObject);

              });



            });
      });

  });
  });
  
  
});