"use strict"
var extensionOjs = require("./extensionObjs");
var Graph = require("./graph");
var Line = extensionOjs.line;
var Station = extensionOjs.station;

module.exports.Line = Line;

Array.prototype.contains = function (obj) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] === obj) {
      return true;
    }
  }
  return false;
}

var lines = ["AB","C"].map(Line.create.bind(Line));

module.exports.lines = lines;
      
 var stations = ["A1","A2","Central1","B2","B1","Central2","C2","C1"].map(Station.create.bind(Station));
      
module.exports.stations = stations;
      
     lines[0].stations = stations.filter(
       function(station){
         return ["A1","A2","Central1","B2","B1"].contains(station.name)
      });
      

      
      lines[1].stations = stations.filter(
       function(station){
         return ["C1","C2","Central2"].contains(station.name)
      });
      
      
      
           
var map = {
  A1:{A2:52.5},
  A2:{A1:52.5,Central1:52.5},
  Central1:{A2:52.5,B2:52.5,Central2:3},
  B2:{B1:52.5,Central1:52.5},
  B1:{B2:52.5},
  Central2:{Central1:3,C2:30},
  C2:{Central2:30,C1:30},
  C1:{C2:30}
  };
   
 var graph = new Graph(map);
 
 
 module.exports.pathFinder = function(origin,destination){
       
       var path = graph.findShortestPath(origin,destination);
       
       var distance = Station.distanceForPath(path,map);
       
       
       return {
         path: path,
         distance: distance
       };
       
       
 } 