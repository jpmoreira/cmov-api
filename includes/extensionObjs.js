'use strict'
module.exports.line = {
	
	create: function(name){		
		var self = Object.create(this);
		self.name = name;
		self.stations = null;
		return self;		
	}
	
}

module.exports.station = {
	create: function(name){
		var self = Object.create(this);
		self.name = name;
		self.adj = {};
		return self; 
	},
	distanceForPath: function(path,map){
		
		var counter = 0;
		
		path.reduce(function(previousValue, currentValue, index, array) {
			
				if(map[previousValue][currentValue] == undefined)return currentValue;
				
				counter += map[previousValue][currentValue];
				
  				return currentValue;
		
		},path[0]);
		
		return counter;
	},
	toGraphStructure: function(){
		
		var self = this;
		return "{" + Object.keys(this.adj).map(function(key){return key+":"+self.adj[key];}) + "}";
			
	
	}
}