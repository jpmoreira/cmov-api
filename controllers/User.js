'use strict';

var url = require('url');
var auth = require('basic-auth');
var models = require("../models");


var User = require('./UserService');

module.exports.userAddCardGet = function userAddCardGet(req, res, next) {
  var username = req.swagger.params['username'].value;
  var number = req.swagger.params['number'].value;
  var expirationMonth = req.swagger.params['expirationMonth'].value;
  var expirationYear = req.swagger.params['expirationYear'].value;
  var code = req.swagger.params['code'].value;
  var cardType = req.swagger.params['cardType'].value;


  var credentials = auth(req);
  if (!credentials || credentials.name != username) {
    res.statusCode = 401;
    res.end();
    return;
  }

  models.User.findOne(
    {
      where: {
        userName: credentials.name,
        password: credentials.pass,
        conductor: false
      }
    }).then(function (user) {

      if (!user) {
        res.statusCode = 401;
        res.end();
        return;
      }

      var result = User.userAddCardGet(username, number, expirationMonth, expirationYear, code, cardType);

      if (typeof result !== 'undefined') {
        result.then(function (value) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(value || {}, null, 2));
        }, function () {
          res.statusCode = 403;
          res.end();
        });
      }
      else
        res.end();

    }, function () {

      res.statusCode = 401;
      res.end();

    });



};

module.exports.userBuyTicketGet = function userBuyTicketGet (req, res, next) {
  var username = req.swagger.params['username'].value;
  var startStationID = req.swagger.params['startStationID'].value;
  var endStationID = req.swagger.params['endStationID'].value;
  var date = req.swagger.params['date'].value;
  var train = req.swagger.params['train'].value;
  var cardNumber = req.swagger.params['cardNumber'].value;
  var fake = req.swagger.params['fake'].value;
  


  var credentials = auth(req);
  if (!credentials || credentials.name != username) {
    res.statusCode = 401;
    res.end();
    return;
  }

  models.User.findOne(
    {
      where: {
        userName: credentials.name,
        password: credentials.pass,
        conductor: false
      }
    }).then(function (user) {

      if (!user) {
        res.statusCode = 401;
        res.end();
        return;
      }

      var result = User.userBuyTicketGet(username, startStationID, endStationID, date, train, cardNumber,fake);

      if (typeof result !== 'undefined') {

        result.then(function (ticket) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(ticket || {}, null, 2));
        }, function () {

          res.statusCode = 403;
          res.end();
        });

      }
      else
        res.end();

    }, function () {

      res.statusCode = 401;
      res.end();

    });



};

module.exports.userInfoGet = function userInfoGet(req, res, next) {
  var username = req.swagger.params['username'].value;


  var credentials = auth(req);
  if (!credentials || credentials.name != username) {
    res.statusCode = 401;
    res.end();
    return;
  }

  models.User.findOne(
    {
      where: {
        userName: credentials.name,
        password: credentials.pass
      }
    }).then(function (user) {

      if (!user) {
        res.statusCode = 401;
        res.end();
        return;
      }

      var result = User.userInfoGet(username);

      if (typeof result !== 'undefined') {

        result.then(function (value) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(value || {}, null, 2));
        });
      }
      else
        res.end();

    }, function () {

      res.statusCode = 401;
      res.end();

    });


};


module.exports.userRegisterGet = function userRegisterGet(req, res, next) {
  var username = req.swagger.params['username'].value;
  var password = req.swagger.params['password'].value;


  var result = User.userRegisterGet(username, password);

  if (typeof result !== 'undefined') {
    result.then(function (values) {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({} || {}, null, 2));
    }, function () {

      res.statusCode = 403;
      res.end();
    });

  }
  else
    res.end();
};

module.exports.userRemoveCardGet = function userRemoveCardGet(req, res, next) {
  var number = req.swagger.params['number'].value;



  var credentials = auth(req);
  if (!credentials) {
    res.statusCode = 401;
    res.end();
    return;
  }

  models.User.findOne(
    {
      where: {
        userName: credentials.name,
        password: credentials.pass,
        conductor: false
      },
      include:[{model:models.Card, as: "cards"}]
    }).then(function (user) {

      if (
        !user ||
         user.cards.filter(function(card){
           return card.number === number
           }).length == 0
      ) {
        res.statusCode = 401;
        res.end();
        return;
      }
      

      var result = User.userRemoveCardGet(number);

      if (typeof result !== 'undefined') {

        result.then(function (value) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(value || {}, null, 2));
        });

      }
      else
        res.end();

    }, function (error) {

      res.statusCode = 401;
      res.end();

    });




};

module.exports.userTicketsGet = function userTicketsGet(req, res, next) {
  var username = req.swagger.params['username'].value;


  var credentials = auth(req);
  if (!credentials || credentials.name != username) {
    res.statusCode = 401;
    res.end();
    return;
  }

  models.User.findOne(
    {
      where: {
        userName: credentials.name,
        password: credentials.pass,
        conductor: false
      }
    }).then(function (user) {

      if (!user) {
        res.statusCode = 401;
        res.end();
        return;
      }

      var result = User.userTicketsGet(username);

      if (typeof result !== 'undefined') {

        result.then(function (value) {
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(value || {}, null, 2));
        });

      }
      else
        res.end();

    }, function () {

      res.statusCode = 401;
      res.end();

    });

};


