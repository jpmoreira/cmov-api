'use strict';

var models = require('../models');
var Ticket = models.Ticket;
var modelExtensions = require("../includes/modelExtensions.js");
var lines = modelExtensions.lines;
var stations = modelExtensions.stations;
var Promise = require('bluebird');

exports.trainIdGet = function (lineID) {
  
  
  
  var parameters = { 
    attributes: ['id', 'startTime','line'], 
    raw: true 
    };
  
  if(lineID != -1){
    parameters.where = {line:lineID};
  }
  
  
  
  return new Promise(function(accept,reject){

    models.Train.findAll(parameters).then(function(trains){
      
      
      trains.forEach(function(train){train.line = {name:train.line}});
      
      
      accept(trains);
    },reject);    
    
    
  });

 
    

}
exports.trainNameGet = function (name) {


  return models.Train.findAll({ attributes: ['id', 'startTime','line'], raw: true,where:{line:name} });

  /*
    var examples = {};
    
    examples['application/json'] = [ {
    "line" : {
      "distances" : [ "" ],
      "id" : "aeiou",
      "stations" : [ {
        "name" : "aeiou",
        "id" : "aeiou"
      } ]
    },
    "startTime" : "aeiou",
    "id" : "aeiou"
  } ];
    
  
    
    if(Object.keys(examples).length > 0)
      return examples[Object.keys(examples)[0]];
      
      */

}
