'use strict';

var url = require('url');
var Promise = require('bluebird');


var Train = require('./TrainService');


module.exports.trainIdGet = function trainIdGet (req, res, next) {
  var lineID = req.swagger.params['lineID'].value;
  

  var result = Train.trainIdGet(lineID);


  if(typeof result !== 'undefined') {
    
    result.then(function(value){
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(value || {}, null, 2));
    })
  }
  else
    res.end();
};

module.exports.trainNameGet = function trainNameGet (req, res, next) {
  var name = req.swagger.params['name'].value;
  

  var result = Train.trainNameGet(name);

  if(typeof result !== 'undefined') {
    result.then(function(value){
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(value || {}, null, 2));
    });
  }
  else
    res.end();
};
