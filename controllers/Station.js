'use strict';

var url = require('url');


var Station = require('./StationService');


module.exports.stationLineGet = function stationLineGet (req, res, next) {
  var lineID = req.swagger.params['lineID'].value;
  

  var result = Station.stationLineGet(lineID);

  if (typeof result !== 'undefined') {
    
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(result || {}, null, 2));

  }
  else
    res.end();
};
