'use strict';

var fs = require('fs');
var crypto = require('crypto');

var models = require('../models');
var Promise = require('bluebird');
var modelExtensions = require("../includes/modelExtensions.js");


// API - Add user credit card ===========================
exports.userAddCardGet = function(username, number, expirationMonth, expirationYear, code, cardType) {
  
  return new Promise(function (resolve, reject) {

    models.User.findOne({
      where: {
        userName: username
      },
      raw: true
    }).then(function (user) {

      models.Card.create({
        code: code,
        type: cardType,
        number: number,
        expirationYear: expirationYear,
        expirationMonth: expirationMonth,
        user_id: user.id
      }).then(resolve,reject);

    },reject);
    
  });
  
}


exports.userBuyTicketGet = function(username, startStationID, endStationID, date, train, cardNumber,fake) {




return new Promise(function(accept,reject){
  
  models.Card.findOne(
    {
      include:[{
       model: models.User,
       as: "user",
       where: {
         userName:username
       } 
      }],
      where:{
        number: cardNumber
      }
  }).then(function(card){
    
    if(card == null) return reject();
    
    models.Train.findOne({
      where:{
        id:train
      }
    },reject).then(function(train){
      
      if(train == true) return reject();
      
      var path = modelExtensions
      .pathFinder(startStationID,endStationID);
      
      if (path == undefined) return reject();
      
      // Ticket Signature
      var privatePem = fs.readFileSync(process.env.PWD + '/includes/certs/private.pem');
      var privKey = privatePem.toString();
      //var publicPem = fs.readFileSync(process.env.PWD + '/includes/certs/cmov.pub');
      //var pubKey = publicPem.toString();
      

      
      var params = {
        code:"",
        price: path.distance * 5,
        date: date,
        startStation: startStationID,
        endStation: endStationID
      };
      
      if(fake){
        
        
        var ticket = models.Ticket.build(params);
        
         ticket.startStation = {name:ticket.startStation};
         ticket.endStation = {name:ticket.endStation};
       
         ticket.price = parseInt(ticket.price);
         accept(ticket);
      
      }
      else{
        
        models.Ticket.create(params).then(function(ticket){
          
                
          var sign = crypto.createSign('RSA-SHA1');

          sign.update(ticket.id + ticket.startStation + ticket.endStation);

          var signMsg = sign.sign(privKey,'base64');

          ticket.set("code",signMsg);
          ticket.save();

          if (ticket == null) return reject();
          ticket.setUser(card.user);
          ticket.setTrain(train);
          ticket.startStation = { name: startStationID };
          ticket.endStation = { name: endStationID };
          ticket.price = parseInt(ticket.price);
          accept(ticket);


        }, reject);
        
      }
    });  
  },reject);
  
});
 
}

exports.userInfoGet = function(username) {


  return new Promise(function(accept,reject){
    models.User.findOne(
      { attributes: ["userName","password","conductor"],
        where:{"userName":username},
        include: [{model:models.Card, as: "cards", raw: true}, {model:models.Ticket, as: "tickets",include:[{model:models.Train, as: "train"}]}]
      }
  ).then(function(user){
    
    user.tickets.forEach(function(ticket){
      ticket.startStation = {name:ticket.startStation};
      ticket.endStation = {name:ticket.endStation};
      ticket.train.line = {name:ticket.train.line};
      
    });
    
    accept(user);
    
  },reject);
    
    
  }); 
}

exports.userRegisterGet = function(username, password) {


  
  return new Promise(function(resolve,reject){
    
    models.User.findOne({where:{userName:username}}).then(function(user){
    
    

    if (user != null){
      reject();
      return;
    }
    
    models.User.create({"userName":username, "password":password,"conductor":false}).then(resolve).catch(reject);
    
  });
  

  
});
};


// API - Remove user credit card ===========================
exports.userRemoveCardGet = function(number) {
  
  return new Promise(function(resolve,reject){
    
    models.Card.destroy({
      where:{
        number: number
      },
      raw: true
    }).then(resolve);
    
  });  
  
}

// API - get user tickets providing user name
exports.userTicketsGet = function(username) {
    
  return new Promise(function(accept,reject){
  
  
    models.Ticket.findAll({
    include:[{
      attributes: [],
      model: models.User,
      as:'user',
      where:{
        userName: username
      }
    },{
      model: models.Train,
      as: 'train'
    }],
    raw: false 
  }).then(function(tickets){
    
    tickets.forEach(function(ticket){
         ticket.startStation = {name:ticket.startStation};
          ticket.endStation = {name:ticket.endStation};
          ticket.train.line = {name: ticket.train.line};
    });
    accept(tickets);
  },reject);
        
  
  
  
  });

  
}
