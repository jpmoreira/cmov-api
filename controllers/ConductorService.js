'use strict';
var models = require('../models/');
var Promise = require('bluebird');
var network = require('../includes/modelExtensions');



function get_time_difference_string(earlierString,laterString){
  
  
  
  var dates = [earlierString,laterString].map(function(str){
    
    var parts = str.split(":");
    
    var date = new Date();
    
    date.setHours(parts[0]);
    date.setMinutes(parts[1]);
    if(date.length > 2){
      date.setSeconds(parts[2]);  
    }
    
    return date;
    
  });
  return get_time_difference(dates[0],dates[1]);
  
  
}

function get_time_difference(earlierDate,laterDate)
{
  
  
  
  
  
       var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
       var oDiff = new Object();
 
       oDiff.days = Math.floor(nTotalDiff/1000/60/60/24);
       nTotalDiff -= oDiff.days*1000*60*60*24;
 
       oDiff.hours = Math.floor(nTotalDiff/1000/60/60);
       nTotalDiff -= oDiff.hours*1000*60*60;
 
       oDiff.minutes = Math.floor(nTotalDiff/1000/60);
       nTotalDiff -= oDiff.minutes*1000*60;
 
       oDiff.seconds = Math.floor(nTotalDiff/1000);
 
       return oDiff;
 
}




exports.conductorTicketsGet = function(trainID, date) {



  var theDate = new Date(date);

  return new Promise(function(accept,reject){
    models.Ticket.findAll({
    attributes:[
      "code","date","verified","startStation","endStation","id"
    ],
    where:{
      date: theDate
    },
    include:{
      model: models.Train,
      as:"train",
      where:{
        id:trainID
      }
      
    }
    
    
  }).then(function(tickets){
    
    tickets.forEach(function(ticket){
      
      ticket.train.line = {name:ticket.train.line};
      ticket.startStation = {name:ticket.startStation};
      ticket.endStation = {name:ticket.endStation};
      
    });
    accept(tickets);
    
  },reject);
  });

}

exports.conductorUploadTickets = function (tickets) {

  return new Promise.map(tickets, function (ticket) {

    models.Ticket.update({
      verified: 'true'
    }, {
        where: {
          id: ticket
        }
      });

  });

}
