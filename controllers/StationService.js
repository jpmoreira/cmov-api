'use strict';

var models = require('../models');
var modelExtensions = require('../includes/modelExtensions'); 

exports.stationLineGet = function(lineID) {
  
  if(lineID == undefined){
     return modelExtensions.lines.reduce(function(current,line){
       return current.concat(line.stations);
     },[]);
  }
  
  return modelExtensions.lines.filter(function(value){
     return value.name === lineID;
    })[0].stations;
  
}
