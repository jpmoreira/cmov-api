'use strict';

var url = require('url');
var Conductor = require('./ConductorService');



var auth = require('basic-auth');
var models = require('../models/');


module.exports.conductorTicketsGet = function conductorTicketsGet (req, res, next) {
  var trainID = req.swagger.params['trainID'].value;
  var date = req.swagger.params['date'].value;
  
  var credentials = auth(req);
  if(!credentials){
    res.statusCode = 401;
    res.end();
    return; 
  }
  
  models.User.findOne(
    {
      where:{
        userName:credentials.name,
        password:credentials.pass,
        conductor:true
      }
    }).then(function(user){
      
      if(!user){
        res.statusCode = 401;
        res.end();
        return;
      }
    
      var result = Conductor.conductorTicketsGet(trainID, date);

  if(typeof result !== 'undefined') {
    
    result.then(function(value){
      
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(value || {}, null, 2));
      
      
      
    },function(){
      
      res.statusCode = 401;
      res.end();
      
      
    });
    
  }
  else
    res.end();
    
  },function(){
    
    req.end();
    
  });
  
  


};

module.exports.conductorUploadTickets = function conductorUploadTickets(req, res, next) {
  var tickets = req.swagger.params['tickets'].value;


/*
var credentials = auth(req);
  if(!credentials){
    res.statusCode = 401;
    res.end();
    return; 
  }
  
  models.User.findOne(
    {
      where:{
        userName:credentials.name,
        password:credentials.pass,
        conductor:true
      }
    }).then(function(user){
      
       if(!user){
        res.statusCode = 401;
        res.end();
        return;
      }
      
    },function(){
      
       res.statusCode = 401;
       res.end();
      
    });
*/



var credentials = auth(req);
  if(!credentials){
    res.statusCode = 401;
    res.end();
    return; 
  }
  
  models.User.findOne(
    {
      where:{
        userName:credentials.name,
        password:credentials.pass,
        conductor:true
      }
    }).then(function(user){
      
      var result = Conductor.conductorUploadTickets(tickets);

  if (typeof result !== 'undefined') {
    result.then(function (value) {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(value || {}, null, 2));
    });

  }
  else
    res.end();
      
    },function(){
      
       res.statusCode = 401;
       res.end();
      
    });



  
};
