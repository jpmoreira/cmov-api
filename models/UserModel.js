"use strict";

module.exports = function(sequelize, DataTypes) {
 
  var User = sequelize.define('User', {
  id:{
    type: DataTypes.INTEGER,
    field: 'id',
    allowNull: false,
    unique: 'user_id',
    primaryKey: true,
    autoIncrement: true
  },
  userName: {
    type: DataTypes.STRING,
    field: 'userName', // Will result in an attribute that is firstName when user facing but first_name in the database
    allowNull: false,
    unique: 'usernameIndex'
  },
  password: {
    type: DataTypes.STRING,
    field: 'pw',
    allowNull: false
  },
  conductor: {
    type: DataTypes.BOOLEAN,
    field: 'conductor',
    defaultValue: false
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});
  
  return User;
  
 
  
};


