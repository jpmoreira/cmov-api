"use strict";

module.exports = function(sequelize, DataTypes) {

  var Ticket = sequelize.define('Ticket', {
  id:{
    type: DataTypes.INTEGER,
    field: 'id',
    allowNull: false,
    unique: 'ticket_id',
    primaryKey: true,
    autoIncrement: true
  },
  code: {
    type: DataTypes.STRING,
    field: 'code',
    allowNull: true
  },
  price: {
    type: DataTypes.INTEGER,
    field: 'price',
    allowNull: false
  },
  date: {
    type: DataTypes.DATEONLY,
    field: 'date',
	allowNull: false
  },
  verified:{
    type: DataTypes.BOOLEAN,
    field: 'verified',
    defaultValue: false
  },
  startStation:{
    type: DataTypes.STRING,
    field: 'startStation',
    allowNull: false
  },
  endStation:{
    type: DataTypes.STRING,
    field: 'endStation',
    allowNull: false
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});
  
  return Ticket;
  
 
  
};

/*
 id:
 type: string
 code:
 type: string
 startStation:
 $ref: '#/definitions/Station'
 endStation:
 $ref: '#/definitions/Station'
 price:
 type: integer
 date:
 type: string
 train:
 $ref: '#/definitions/Train'
 verified:
 type: boolean
 */