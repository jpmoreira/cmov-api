module.exports = function(sequelize, DataTypes) {
 
  var Card = sequelize.define('Card', {
  id:{
    type: DataTypes.INTEGER,
    field: 'id',
    allowNull: false,
    unique: 'card_id',
    primaryKey: true,
    autoIncrement: true
  },
  code: {
    type: DataTypes.STRING,
    field: 'code', // Will result in an attribute that is firstName when user facing but first_name in the database
    allowNull: false
  },
  type: {
	  type: DataTypes.STRING,
	  field: 'type',
	  allowNull: false
  },
  number: {
    type: DataTypes.STRING,
    field: 'number',
    allowNull: false,
    unique: 'card_number'
  },
  expirationYear: {
	  type: DataTypes.STRING,
	  field: 'expirationYear',
	  allowNull: false
  },
  expirationMonth:{
    type: DataTypes.STRING,
    field: 'expirationMonth',
    allowNull: false
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});
  
  
  return Card;
 
};