"use strict";

module.exports = function(sequelize, DataTypes) {

  var Train = sequelize.define('Train', {
  id:{
    type: DataTypes.INTEGER,
    field: 'id',
    allowNull: false,
    unique: 'train_id',
    primaryKey: true,
    autoIncrement: true
  },
  startTime: {
    type: DataTypes.TIME,
    field: 'time',
	allowNull: false
  },
  line: {
    type: DataTypes.STRING,
    field: 'line',
    allowNull: false
  },
  direction: {
    type: DataTypes.BOOLEAN,
    field: 'direction',
    defaultValue: DataTypes.TRUE,
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});
  
  return Train;
  
 
  
};

