"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

/*
db.Line.belongsToMany(db.Station,{through:'Station_Line',as:'stations'});
db.Station.belongsToMany(db.Line,{through:'Station_Line',as:'lines'});
*/
// USER - CARD
db.User.hasMany(db.Card,{as: 'cards',foreignKey:"user_id"});
db.Card.belongsTo(db.User, {as:'user', foreignKey:"user_id"});

// USER - TICKET
db.User.hasMany(db.Ticket, {as: 'tickets', foreignKey:"user_id"});
db.Ticket.belongsTo(db.User, {as:'user', foreignKey:"user_id"});

// TICKET - TRAIN
db.Ticket.belongsTo(db.Train, {as: 'train', foreignKey:"train_id"});
db.Train.hasMany(db.Ticket,{as: 'tickets', foreignKey:"train_id"});

//db.Station.hasMany(db.Ticket,{as:"startStation", foreignKey:"start_station"});
//db.Station.hasMany(db.Ticket,{as:"endStation", foreignKey:"end_station"});
//db.Line.hasOne(db.Train,{as: 'line'});

//db.Train.belongsTo(db.Line,{as: 'line'});
//db.Train.belongsTo(db.Line,{as: 'line',foreignKey:"lineID"});
//db.Line.hasMany(db.Train,{as: 'trains',foreignKey:"lineID"});
module.exports = db;